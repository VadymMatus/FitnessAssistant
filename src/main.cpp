/***
 * Вункция main будет только реализовывать общение между модулями программы,
 * весь функционал программы разпределён в разных модулях.
 * Главный модуль програмы, это же и основное меню см.StaticLib/WinMain.cpp,
 * он будет использоваться программой в виде динамически подключаемой библиотеке
 * см.SharedLib/libWinMain.so
 */

#include <iostream>
#include <fstream>
#include <gtkmm/application.h>
#include <gtkmm/main.h>
#include "StaticLib/WinMain.hpp"
#include "StaticLib/WinInitData.hpp"
#include "StaticLib/FitnessAssistant.hpp"

/***
 * Класс Login реализует окно входа в систему, где будет ввод имени пользователя
 * и пароля. Если в систему входит обычный пользователь, то ему доступен только
 * его профиль. При входе с под прав администратора, доступно редактирование всех
 * даных, всех зарегестрированых пользователей.
 */
class Login : public Gtk::Window
{
  public:
    Login();

  private:
    /***
     * Функция set_list_user(), считывает имена всех зарезервированых 
     * пользователей из файла FILE_USER_INIT_DATA и добавляет их в подсказку,
     * которая будет отображаться при поиске.
     */
    void set_list_user();

    /* Завершение роботы */
    void on_button_quit();

    /**
     * Функция-обратотчик on_button_login(), сравнивает введеное имя и пароль
     * со всеми зарезервироваными пользователями в системе, и в следствии чего
     * производиться вход в систему под обычным пользователем или как админом.
     * Если пользователь ненайден, вызываеться функция usr_does_not_exist() 
     */
    void on_button_login();

    /* Функция выводит сообщение о том что пользователь необнаружен в системе */
    void usr_does_not_exist();

    /**
     * Функция-обратотчик on_button_autorization(),вызывает модуль WinInitData,
     * запускает ввода начальных данных. После сохранения данных, появляеться
     * окно авторизации, где новый пользователь может зайти под своим именем. 
     */
    void on_button_autorization();

    /* Реализует вывод подсказки, в поле ввода имени пользователя */
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
      public:
        ModelColumns()
        {
            add(m_col_name);
        }

        Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    };

    ModelColumns m_Columns;
    typedef std::map<int, Glib::ustring> type_actions_map;
    type_actions_map m_CompletionActions;

    Gtk::Box m_VBox_Main, m_HBox_Account, m_HBox_Passwd;
    Gtk::ButtonBox m_ButtonBox_Bottom;
    Gtk::Entry m_entry_account, m_entry_passwd;
    Gtk::Label m_label_account, m_label_passwd;
    Gtk::Button m_button_login, m_button_quit, m_button_autorization;
};

int main(int argc, char *argv[])
{
    Gtk::Main kit(argc, argv);

    /********** Проверка, вводил ли, пользователь данные о себе **********/
    if (!(ass::FileExists(FILE_USER_INIT_DATA)) or (ass::FileEmpty(FILE_USER_INIT_DATA)))
    {
        WinInitData ObjWinInitData(-1, true, true);
        WinMain ObjWinMain(-1, true);
        // Gtk::Main::run(ObjWinInitData);
        // Gtk::Main::run(ObjWinMain);
    }
    else
    {
        // WinMain ObjWinMain(1, false);
        // Gtk::Main::run(ObjWinMain);

        Login ObjLogin;
        Gtk::Main::run(ObjLogin);
    }

    return 0;
}

Login::Login()
    : m_VBox_Main(Gtk::ORIENTATION_VERTICAL, 5),
      m_HBox_Account(Gtk::ORIENTATION_HORIZONTAL),
      m_HBox_Passwd(Gtk::ORIENTATION_HORIZONTAL),
      m_button_quit(resources.exit),
      m_button_login(resources.comin),
      m_button_autorization(resources.registry),
      m_label_account(resources.user_record + ": "),
      m_label_passwd(resources.user_passwd + ": ")
{
    this->set_title(resources.login);
    this->set_resizable(false);
    this->set_border_width(BORDER_WIGHT_BOX);
    this->set_default_size(350, 0);
    this->add(m_VBox_Main);
    this->set_position(Gtk::WIN_POS_CENTER);

    set_list_user();

    m_HBox_Account.pack_start(m_label_account);
    m_HBox_Account.pack_start(m_entry_account);
    m_entry_account.set_max_length(55);

    m_HBox_Passwd.pack_start(m_label_passwd);
    m_HBox_Passwd.pack_start(m_entry_passwd);
    m_entry_passwd.set_visibility(false);
    m_entry_passwd.set_max_length(55);

    m_ButtonBox_Bottom.pack_start(m_button_quit);
    m_ButtonBox_Bottom.pack_start(m_button_autorization);
    m_ButtonBox_Bottom.pack_start(m_button_login);

    m_VBox_Main.pack_start(m_HBox_Account, Gtk::PACK_SHRINK);
    m_HBox_Account.set_border_width(2);

    m_VBox_Main.pack_start(m_HBox_Passwd, Gtk::PACK_SHRINK);
    m_HBox_Passwd.set_border_width(2);

    m_VBox_Main.pack_start(m_ButtonBox_Bottom, Gtk::PACK_SHRINK);

    m_button_login.signal_clicked().connect(sigc::mem_fun(*this, &Login::on_button_login));
    m_entry_account.signal_activate().connect(sigc::mem_fun(*this, &Login::on_button_login));
    m_entry_passwd.signal_activate().connect(sigc::mem_fun(*this, &Login::on_button_login));
    m_button_autorization.signal_clicked().connect(sigc::mem_fun(*this, &Login::on_button_autorization));
    m_button_quit.signal_clicked().connect(sigc::mem_fun(*this, &Login::on_button_quit));

    this->show_all_children();
}

void Login::set_list_user()
{
    ass::InitData *temp = new ass::InitData;
    ass::root *root = new ass::root;
    size_t size = sizeof(ass::InitData);
    auto completion = Gtk::EntryCompletion::create();

    auto refCompletionModel = Gtk::ListStore::create(m_Columns);
    completion->set_model(refCompletionModel);

    m_entry_account.set_completion(completion);

    if (!(ass::FileExists(FILE_USER_INIT_DATA)))
    {
        std::ofstream ofs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::out);
        ofs.write((char *)root, sizeof(ass::root));
        ofs.close();
    }

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);

    Gtk::TreeModel::Row row = *(refCompletionModel->append());
    ifs.read((char *)root, sizeof(ass::root));
    row[m_Columns.m_col_name] = root->name;

    while (ifs.read((char *)temp, size))
    {
        row = *(refCompletionModel->append());
        row[m_Columns.m_col_name] = temp->Name;
    }

    ifs.close();

    completion->set_text_column(m_Columns.m_col_name);

    delete temp;
    delete root;
}

void Login::on_button_quit()
{
    this->hide();
}

void Login::on_button_login()
{
    ass::InitData *temp = new ass::InitData;
    ass::root *root = new ass::root;
    size_t size = sizeof(ass::InitData);
    bool usr_exist(false);
    int index(0);

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
    ifs.read((char *)root, sizeof(ass::root));

    if (strcmp(root->name, m_entry_account.get_text().c_str()) == 0 and strcmp(root->passwd, m_entry_passwd.get_text().c_str()) == 0)
    {
        usr_exist = true;
        Login::on_button_quit();
        WinMain ObjWinMain(-1, true);
        Gtk::Main::run(ObjWinMain);
    }
    else
    {
        while (ifs.read((char *)temp, size))
            if (strcmp(temp->Name, m_entry_account.get_text().c_str()) == 0 and strcmp(temp->Passwd, m_entry_passwd.get_text().c_str()) == 0)
            {
                usr_exist = true;
                Login::on_button_quit();
                WinMain ObjWinMain(index, false);
                Gtk::Main::run(ObjWinMain);

                break;
            }
            else
                index++;
    }

    ifs.close();

    if (usr_exist == false)
        Login::usr_does_not_exist();

    delete root;
    delete temp;
}

void Login::usr_does_not_exist()
{
    Gtk::MessageDialog m_MessageDialog(resources.error);
    m_MessageDialog.set_secondary_text(resources.invalid_name_or_passwd);
    m_MessageDialog.run();
}

void Login::on_button_autorization()
{
    WinInitData ObjWinInitData(-1, false, true);

    this->set_sensitive(false);
    Gtk::Main::run(ObjWinInitData);
    this->set_sensitive(true);

    Login::set_list_user();
}