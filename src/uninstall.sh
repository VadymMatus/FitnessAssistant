#!/bin/sh

if [ "$(id -u)" != "0" ]; then
	echo "Для удаления приложения вы должны овладать правами супер пользовалетя!" 
	exit 0
fi

rm /usr/bin/fitness-assistant
rm -r /usr/share/FitnessAssistant/
rm /usr/share/applications/'Fitness Assistant'.desktop
rm /usr/share/pixmaps/fitness-assistant.png
echo "Программа полностью удаленна с вашего ПК!"
