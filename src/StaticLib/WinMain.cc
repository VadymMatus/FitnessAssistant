#include "WinMain.hpp"

WinMain::WinMain(int n_usr, bool is_root)
    : nUserInit(n_usr), isRoot(is_root),
      winNutrition(nUserInit, isRoot),
      winStatistic(nUserInit, isRoot)
{
    /**********  Параметры окна  **********/
    this->set_position(Gtk::WIN_POS_CENTER);
    this->set_title(resources.title_main);
    this->set_default_size(1000, 800);

    m_Notebook.append_page((new WinTrening(isRoot))->getScrolledWindow(), resources.trening);
    m_Notebook.append_page(*(winNutrition.getScrolledWindow()), resources.tab_food);
    m_Notebook.append_page(*(winStatistic.getScrolledWindow()), resources.statistic);
    m_Notebook.append_page((new WinMain::WinSetting(nUserInit, isRoot))->getScrolledWindow(), resources.setting, true);

    this->add(m_Notebook);
    this->show_all_children();
    // Gtk::Main::run(*this);
}

WinMain::WinSetting::WinSetting(int n_usr, bool is_root)
    : nUserInit(n_usr), isRoot(is_root),
      m_button_change_init_data(resources.init_data),
      m_button_new_user(resources.new_user),
      m_Label_Sorting(resources.sorted + ":"),
      m_Label_filter(resources.filter + ":"),
      m_Label_inquiry(resources.percentage_ratio + ":"),
      m_frame_change_language(resources.change_language)
{
    /********** Главное *********/
    m_Notebook.append_page(m_ScrolledWindow_Setting, resources.main);

    /**********  Добавление виджетов окна настроек **********/
    m_ScrolledWindow_Setting.set_border_width(6);
    m_ScrolledWindow_Setting.add(m_VBox_Setting);
    m_VBox_Setting.set_spacing(6);

    if (isRoot == true) // только root можно создавать пользователей
    {
    m_frame_change_language.add(*(new ChangeLanguage()));
    m_VBox_Setting.pack_start(m_frame_change_language, Gtk::PACK_SHRINK, 0);

        m_VBox_Setting.pack_start(m_button_new_user, Gtk::PACK_SHRINK);
        m_button_new_user.signal_clicked().connect(sigc::mem_fun(*this, &WinMain::WinSetting::on_button_new_user));

        /********** ПОЛЬЗОВАТЕЛИ **********/
        m_ScrolledWindow_User.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
        m_Notebook.append_page(m_VBox_User_Main, resources.users);

        m_VBox_User_Main.pack_start(m_ScrolledWindow_User);
        m_ScrolledWindow_User.set_border_width(5);
        m_ScrolledWindow_User.add(m_VBox_User);
        m_VBox_User.pack_start(m_TreeView);
        m_VBox_User_Main.pack_start(m_butonbox_bottom, Gtk::PACK_SHRINK);

        /********** Запрос *********/
        m_comboboxtext_inquiry.append(resources.inquiry_target);
        m_comboboxtext_inquiry.append(resources.inquiry_activity);
        m_comboboxtext_inquiry.append(resources.inquiry_sex);

        m_VBox_inquiry.pack_start(m_Label_inquiry);
        m_VBox_inquiry.pack_start(m_comboboxtext_inquiry);
        m_butonbox_bottom.pack_start(m_VBox_inquiry);

        m_comboboxtext_inquiry.signal_changed().connect(sigc::mem_fun(*this, &WinMain::WinSetting::on_combo_inquiry));

        /********** Фильтрация *********/
        m_comboboxtext_filter.append(resources.sort_activity);
        m_comboboxtext_filter.append(resources.sort_sit_down);
        m_comboboxtext_filter.append(resources.sort_pass);

        m_VBox_filter.pack_start(m_Label_filter);
        m_VBox_filter.pack_start(m_comboboxtext_filter);
        m_butonbox_bottom.pack_start(m_VBox_filter);

        m_comboboxtext_filter.signal_changed().connect(sigc::mem_fun(*this, &WinMain::WinSetting::on_combo_filter));

        /********** Сортировка *********/
        m_comboboxtext_sorting.append(resources.A_z);
        m_comboboxtext_sorting.append(resources.Z_a);
        m_comboboxtext_sorting.append(resources.sort_weight_down);
        m_comboboxtext_sorting.append(resources.sort_weight_up);
        m_comboboxtext_sorting.append(resources.sort_desired_weight_down);
        m_comboboxtext_sorting.append(resources.sort_desired_weight_up);
        m_comboboxtext_sorting.append(resources.sort_age_down);
        m_comboboxtext_sorting.append(resources.sort_age_up);
        m_comboboxtext_sorting.append(resources.sort_growth_down);
        m_comboboxtext_sorting.append(resources.sort_growth_up);

        m_VBox_Sorting.pack_start(m_Label_Sorting);
        m_VBox_Sorting.pack_start(m_comboboxtext_sorting);
        m_butonbox_bottom.pack_start(m_VBox_Sorting);

        m_comboboxtext_sorting.signal_changed().connect(sigc::mem_fun(*this, &WinMain::WinSetting::on_combo_sorting));

        /********** Нижний бокс *********/
        m_butonbox_bottom.set_halign(Gtk::ALIGN_END);

        /********** Параметры таблицы *********/
        m_TreeView.append_column(resources.number, m_Columns->m_col_num);
        m_TreeView.append_column(resources.name, m_Columns->m_col_name);
        m_TreeView.append_column(resources.age, m_Columns->m_col_age);
        m_TreeView.append_column(resources.current_weight, m_Columns->m_col_current_weight);
        m_TreeView.append_column(resources.desired_weight, m_Columns->m_col_desired_weight);
        m_TreeView.append_column(resources.growth, m_Columns->m_col_growth);
        m_TreeView.append_column(resources.sex, m_Columns->m_col_sex);
        m_TreeView.append_column(resources.target, m_Columns->m_col_target);
        m_TreeView.append_column(resources.activity, m_Columns->m_col_activity);
        
        WinMain::WinSetting::fill_table();
    }

    m_VBox_Setting.pack_start(m_button_change_init_data, Gtk::PACK_SHRINK);
    m_button_change_init_data.signal_clicked().connect(sigc::mem_fun(*this, &WinMain::WinSetting::on_button_changed_init_data));

    m_ScrolledWindow.add(m_Notebook);
}

WinMain::WinSetting::ChangeLanguage::ChangeLanguage()
{
    // чтение настроек с файла
    std::ifstream ifs(FILE_PROG_PREFERENCES, std::ios_base::binary | std::ios_base::in);
    ifs >> json;
    ifs.close();

    m_refTreeModel = Gtk::ListStore::create(m_Columns);
    m_Combo.set_model(m_refTreeModel);

    // заполнение бокса с выбором языка
    for (auto &obj : json["supported-languages-full-name"])
    {
        static size_t count(0);

        row = *(m_refTreeModel->append());
        row[m_Columns.m_col_id] = count++;
        row[m_Columns.m_col_name] = obj.get<std::string>();
    }

    // показ текущего языка
    Json languages(json.at("supported-languages-abbreviation")),
        cur_lang(json.at("language"));

    for (size_t i(0); i < languages.size(); i++)
        if (languages.at(i).get<std::string>().compare(cur_lang) == 0)
            m_Combo.set_active(i);

    m_Combo.pack_start(m_Columns.m_col_name);
    m_Combo.pack_start(m_cell, Gtk::PACK_EXPAND_WIDGET);

    m_Combo.signal_changed().connect(sigc::mem_fun(*this, &WinMain::WinSetting::ChangeLanguage::on_combo_changed));

    pack_start(m_Combo, Gtk::PACK_SHRINK, 2);
}

void WinMain::WinSetting::ChangeLanguage::on_combo_changed()
{
    Gtk::TreeModel::iterator iter = m_Combo.get_active();
    if (iter)
    {
        Gtk::TreeModel::Row row = *iter;
        if (row)
        {
            json["language"] = json["supported-languages-abbreviation"][row[m_Columns.m_col_id]];

            std::ofstream ofs(FILE_PROG_PREFERENCES, std::ios_base::binary | std::ios_base::out);
            ofs << json.dump(SPACE_IN_JSON_FILES);
            ofs.close();
        }
    }
}

Gtk::ScrolledWindow &WinMain::WinSetting::getScrolledWindow()
{
    return this->m_ScrolledWindow;
}

void WinMain::WinSetting::on_button_changed_init_data()
{
    WinInitData WinInitDataObj(nUserInit, isRoot, false);
    Gtk::Main::run(WinInitDataObj);
    WinSetting::fill_table();
}

void WinMain::WinSetting::on_button_new_user()
{
    WinInitData WinInitDataObj(nUserInit, isRoot, true);
    Gtk::Main::run(WinInitDataObj);
    WinSetting::fill_table();
}

void WinMain::on_button_quit()
{
    this->hide();
}

void WinMain::WinSetting::fill_table()
{
    ass::InitData *temp = new ass::InitData;
    size_t count(0);

    m_refTreeModel = Gtk::ListStore::create(*m_Columns);
    m_TreeView.set_model(m_refTreeModel);

    m_TreeView.set_reorderable();
    m_TreeView.set_rules_hint();
    m_TreeView.set_headers_clickable(true);
    m_TreeView.set_headers_visible(true);
    m_TreeView.set_search_column(1);

    m_refTreeSelection = m_TreeView.get_selection();

    std::ifstream ifs(FILE_USER_INIT_DATA, std ::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)temp, sizeof(*temp)))
    {
        row = *(m_refTreeModel->append());

        row[m_Columns->m_col_num] = ++count;
        row[m_Columns->m_col_name] = temp->Name;
        row[m_Columns->m_col_age] = temp->Age;
        row[m_Columns->m_col_current_weight] = temp->CurrentWeight;
        row[m_Columns->m_col_desired_weight] = temp->DesiredWeight;
        row[m_Columns->m_col_growth] = temp->Growth;
        row[m_Columns->m_col_sex] = (temp->Sex ? resources.male : resources.female);
        row[m_Columns->m_col_target] = (temp->Target == ass::TargetLoseWeight
                                            ? resources.lose_weight
                                            : temp->Target == ass::TargetSupport ? resources.support_weight : resources.fast);
        row[m_Columns->m_col_activity] = (temp->Activity == ass::ActivitySedentary
                                              ? resources.activity_sit_down
                                              : temp->Activity == ass::ActivityNormal ? resources.activity_normaly : resources.activity_hard);
    }

    delete temp;
    ifs.close();
}

void WinMain::WinSetting::on_combo_sorting()
{
    if (m_comboboxtext_filter.get_active_text().size() != 0 and m_comboboxtext_sorting.get_active_text().size() != 0)
        m_comboboxtext_filter.set_active(-1);

    ass::InitData *temp = new ass::InitData[2];
    size_t size = sizeof(temp[0]);

    std::fstream fs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in | std::ios_base::out);
    fs.seekp(sizeof(ass::root), std::ios_base::beg);

    if (m_comboboxtext_sorting.get_active_text() == resources.A_z)
    {
        while (fs.read((char *)temp, size * 2))
            if (strcmp(temp[0].Name, temp[1].Name) > 0)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.Z_a)
    {
        while (fs.read((char *)temp, size * 2))
            if (strcmp(temp[0].Name, temp[1].Name) < 0)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_weight_down)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].CurrentWeight < temp[1].CurrentWeight)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_weight_up)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].CurrentWeight > temp[1].CurrentWeight)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_age_down)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].Age < temp[1].Age)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_age_up)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].Age > temp[1].Age)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_growth_down)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].Growth < temp[1].Growth)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_growth_up)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].Growth > temp[1].Growth)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_desired_weight_down)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].DesiredWeight < temp[1].DesiredWeight)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }
    else if (m_comboboxtext_sorting.get_active_text() == resources.sort_desired_weight_up)
    {
        while (fs.read((char *)temp, size * 2))
            if (temp[0].DesiredWeight > temp[1].DesiredWeight)
            {
                fs.seekp(size * (-2), std::ios_base::cur);

                fs.write((char *)&temp[1], size);
                fs.write((char *)&temp[0], size);

                fs.seekp(sizeof(ass::root), std::ios_base::beg);
            }
            else
                fs.seekp(-size, std::ios_base::cur);
    }

    delete[] temp;
    fs.close();

    WinSetting::fill_table();
}

void WinMain::WinSetting::on_combo_filter()
{
    if (m_comboboxtext_sorting.get_active_text().size() != 0 and m_comboboxtext_filter.get_active_text().size() != 0)
        m_comboboxtext_sorting.set_active(-1);

    ass::InitData *temp = new ass::InitData;
    size_t count(0);
    short int result = (m_comboboxtext_filter.get_active_text() == resources.sort_sit_down
                            ? ass::ActivitySedentary
                            : m_comboboxtext_filter.get_active_text() == resources.sort_activity
                                  ? ass::ActivityHigh
                                  : ass::ActivityNormal);

    m_refTreeModel = Gtk::ListStore::create(*m_Columns);

    std::ifstream ifs(FILE_USER_INIT_DATA, std ::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)temp, sizeof(*temp)))
    {
        if (temp->Activity != result)
            continue;

        row = *(m_refTreeModel->append());

        row[m_Columns->m_col_num] = ++count;
        row[m_Columns->m_col_name] = temp->Name;
        row[m_Columns->m_col_age] = temp->Age;
        row[m_Columns->m_col_current_weight] = temp->CurrentWeight;
        row[m_Columns->m_col_desired_weight] = temp->DesiredWeight;
        row[m_Columns->m_col_growth] = temp->Growth;
        row[m_Columns->m_col_sex] = (temp->Sex ? resources.male : resources.female);
        row[m_Columns->m_col_target] = (temp->Target == ass::TargetLoseWeight
                                            ? resources.lose_weight
                                            : temp->Target == ass::TargetSupport ? resources.support_weight : resources.fast);
        row[m_Columns->m_col_activity] = (temp->Activity == ass::ActivitySedentary
                                              ? resources.activity_sit_down
                                              : temp->Activity == ass::ActivityNormal ? resources.activity_normaly : resources.activity_hard);
    }

    delete temp;
    ifs.close();
}

void WinMain::WinSetting::on_combo_inquiry()
{
    Gtk::TreeModel::iterator store_iter = m_refTreeSelection->get_selected();


    if (m_comboboxtext_inquiry.get_active_text().size() == 0)
        return;

    Gtk::MessageDialog ObjMssDialog();
    ass::InitData *temp = new ass::InitData;
    int count = NumEntriesFile(FILE_USER_INIT_DATA, *temp);
    std::stringstream ss;
    int percentages[3] = {0};

    std::ifstream ifs(FILE_USER_INIT_DATA, std ::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    Gtk::MessageDialog m_MessageDialog(resources.result, false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK);

    if (m_comboboxtext_inquiry.get_active_text() == resources.inquiry_target)
    {
        while (ifs.read((char *)temp, sizeof(*temp)))
            switch (temp->Target)
            {
            case ass::TargetLoseWeight:
                percentages[0]++;
                break;
            case ass::TargetSupport:
                percentages[1]++;
                break;
            case ass::TargetGetFat:
                percentages[2]++;
                break;
            }

        ss << floor(((float)percentages[0] / count) * 100);
        m_MessageDialog.set_secondary_text(
            resources.percentage_ratio_target + ":\n" +
            "     — " + resources.need_lose_weight + " " +
            ss.str() + "%\n");
        ss.str("");

        ss << floor(((float)percentages[1] / count) * 100);
        m_MessageDialog.set_secondary_text(
            m_MessageDialog.property_secondary_text() +
            "     — " + resources.need_support_weight + " " + ss.str() + "%\n");
        ss.str("");

        ss << floor(((float)percentages[2] / count) * 100);
        m_MessageDialog.set_secondary_text(
            m_MessageDialog.property_secondary_text() +
            "     — " + resources.need_fast + "  " + ss.str() + "%");
        ss.str("");
    }
    else if (m_comboboxtext_inquiry.get_active_text() == resources.inquiry_activity)
    {
        while (ifs.read((char *)temp, sizeof(*temp)))
            switch (temp->Activity)
            {
            case ass::ActivitySedentary:
                percentages[0]++;
                break;
            case ass::ActivityNormal:
                percentages[1]++;
                break;
            case ass::ActivityHigh:
                percentages[2]++;
                break;
            }

        ss << floor(((float)percentages[0] / count) * 100);
        m_MessageDialog.set_secondary_text(
            resources.percentage_ratio_activity + ":\n" +
            "     — " + resources.loos_people + " " +
            ss.str() + "%\n");
        ss.str("");

        ss << floor(((float)percentages[1] / count) * 100);
        m_MessageDialog.set_secondary_text(
            m_MessageDialog.property_secondary_text() +
            "     — " + resources.sort_activity + " " +
            ss.str() + "%\n");
        ss.str("");

        ss << floor(((float)percentages[2] / count) * 100);
        m_MessageDialog.set_secondary_text(
            m_MessageDialog.property_secondary_text() +
            "     — " + resources.hard_people + "  " + ss.str() + "%");
        ss.str("");
    }
    else if (m_comboboxtext_inquiry.get_active_text() == resources.inquiry_sex.c_str())
    {
        while (ifs.read((char *)temp, sizeof(*temp)))
            switch (temp->Sex)
            {
            case true:
                percentages[0]++;
                break;
            case false:
                percentages[1]++;
                break;
            }
        ss << floor(((float)percentages[0] / count) * 100);
        m_MessageDialog.set_secondary_text(
            resources.percentage_ratio_sex + ":\n" +
            "     " + resources.percentage_ratio_male + " " +
            ss.str() + "%\n");

        ss.str("");

        ss << floor(((float)percentages[1] / count) * 100);
        m_MessageDialog.set_secondary_text(
            m_MessageDialog.property_secondary_text() +
            "     " + resources.percentage_ratio_female + " " +
            ss.str() + "%\n");
    }

    delete temp;
    ifs.close();

    m_MessageDialog.run();

    m_comboboxtext_inquiry.set_active(-1);
}