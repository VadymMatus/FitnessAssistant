// -*- C++ -*-
#ifndef FITNESSASSISTANT_HPP
#define FITNESSASSISTANT_HPP

// #define NDEBUG

#include <iostream>
#include <fstream>
#include <gtkmm.h>
#include "json.hpp"
#include <assert.h>

using Json = nlohmann::json;

/**
 * Пространство имён ass, содержит основные функциии и перечисления
 * для модулей программы FitnessAssistant и структуры, такие как
 * проверка файла на пустоту, проверка файла на существование,
 * и множество полезных функций для удобной работы с файлами и структурами.
 */

/* для разработки */
#define DEBUG
/* размер табуляции в json файлах */
#define SPACE_IN_JSON_FILES 4
/* количество нулей после запятой */
#define NUMBER_OF_ZEROS_AFTER_COMMA 1
/* растояние между кнопками в контейнере BottonBox */
#define SPACING_BUTTON_BOX 10
#define BORDER_WIGHT_BOX 5
#define RU "RU"
#define UK "UK"

/* Имя файла где будут храниться все физические данные о пользователе */
#define FILE_USER_INIT_DATA "../usr/Data/InitData.dat"
/* Имя файла где будут храниться все росчитаные даные см.CaluclData.hpp */
#define FILE_USER_CALCUL_DATA "../usr/Data/CalculData.dat"
#define FILE_USER_CALCUL_DATA_JSON "../usr/Data/CalculData.json"
/* Имя файла где будут храниться все росчитаные даные см.CaluclData.hpp */
#define FILE_USER_STATISTIC_DATA "../usr/Data/Statistic.dat"
#define FILE_USER_STATISTIC_DATA_JSON "../usr/Data/Statistic.json"
/* База данных упражнений для модуля WinTrening */
#define FILE_TRENING_DATA_JSON "DataBase/Trening.json"
/* Файл конфигурации приложения */
#define FILE_PROG_PREFERENCES "Preferences/setting.json"
/* Временные данные */
#define FILE_TEMP "../usr/temp"
/* Папка пользователя */
#define FOLDER_USR "../usr"
/* Папка даных пользователя */
#define FOLDER_USR_DATA "../usr/Data/"
/* Папка временнях даных пользователя */
#define FOLDER_USR_TEMP "../usr/Temp/"
/* Папка конфигурации приложения */
#define FOLDER_PROG_PREFERENCES "Peferefces/"
/* Папка c языковыми пакетами */
#define FOLDER_PROG_LANGUAGE "Language/"

namespace ass
{

/* Список упрощения для структуры UserInitData */
enum ShortUserInitData
{
    TargetLoseWeight = -1,
    TargetSupport = 0,
    TargetGetFat = 1,
    ActivitySedentary = -1,
    ActivityNormal = 0,
    ActivityHigh = 1
};

// enum Target
// {
//     LOSE_WEIRTH = "- 300",
//     SUPPORT = "0",
//     GET_FAT = "+ 300"
// };

/* Структура физических данных пользователя */
struct InitData
{
    char Name[60] = {'\0'}; // имя пользователля
    int Age = 18;           // возраст
    int CurrentWeight = 50; // текущий вес
    int DesiredWeight = 50; // желаемый вес
    int Growth = 150;       // рост
    bool Sex = true;        // пол: true - мужской, false - женский
    int Target = 0;         // цель тренировок(занятий): 1-похудеть, 0-поддержать вес, 1-поправиться
    int Activity = 0;       // днневная активность: -1 - сидячая, 0 - нормальная, 1 - высокая
    char Passwd[60] = {'\n'};
    char Language[4] = RU;
};

struct CalculData
{
    char Name[60];
    float Water = 0.0; // Дневная норма Воды
    int Calories = 0;  // Дневная норма Калорий
    int Fats = 0;      // Деквная норма Жиров
    int Carbon = 0;    // Дневная норма Углеводов
    int Proteins = 0;  // Дневная норма Белков
};

struct root
{
    char name[31] = "admin";
    char passwd[31] = "1111";
};

struct Note : public Gtk::HBox
{
  public:
    Note(std::string name, int weigth, int calories, int protein,
         int fat, int carbon);
    Note();

  private:
    // std::string name, protein, fat, carbon
    Gtk::Label m_label_name, m_label_weigth, m_label_calories,
        m_label_protein, m_label_fat, m_label_carbon;
};

struct Record
{
    char Name[60] = {'\0'};
    int Weigth = 100;
    int Calories = 0;
    int Fats = 0;
    int Carbon = 0;
    int Proteins = 0;
};

struct Statistic
{
    char Date[80] = {'\0'};
    char Name[60] = {'\0'};
    float Water = 0.0f;
    int Calories = 0;
    int Fats = 0;
    int Carbon = 0;
    int Proteins = 0;
    int count = 0;
    ass::Record *record = new ass::Record[20];
};

/***
 * Функция проверяет файл на пустоту.
 * 
 * @param std::string Имя файла
 * @return <tt>true</tt> - файл пустой, <tt>false</tt> - файл не пустой
 */
bool FileEmpty(std::string);

/***
 * Функция проверяет файл на существование.
 * 
 * @param std::string Имя файла
 * @return <tt>true</tt> - файл существует, <tt>false</tt> - файл не существует
 */
bool FileExists(std::string);

/***
 * Функция проверки на правильность ввода числа.
 * 
 * @param number Число для проверки
 * @param message Сообщение для вывода при неудачном вводе
 * 
 * @return коректное число
 */
template <typename T>
void CheckNumEntry(T &number, std::string message)
{
    while (!(std::cin >> number))
    {
        std::cin.clear();
        while (getchar() != '\n')
            continue;

        std::cout << message;
    }

    while (getchar() != '\n')
        continue;
}

/***
 * Функция высчитывает количетво записей структуры в бинарном файле.
 * 
 * @param FileName Имя файла
 * @param Object Обьект струтуры, количество записей которой, нужно посчитать
 * в бинарном файле.
 * 
 * @return Количество записей структуры в бинарном файле
 */
template <class C>
int NumEntriesFile(std::string FileName, C Object)
{
    std::ifstream ifs(FileName, std::ios_base::binary | std::ios_base::ate);
    auto temp = ifs.tellg() / sizeof(Object);
    ifs.close();
    return temp;
}

/***
 * Функция возращает даные о пользователе.
 * 
 * Выполняеться поиск в файле FILE_USER_INIT_DATA, по его порядковому номеру 
 * файле.
 * 
 * @param nUser Порядковый номер пользователя в файле
 * @return Структура с даными пользователя
 */
ass::InitData getInitData(size_t nUser);

/***
 * Функция возращает даные о пользователе.
 * 
 * Выполняеться поиск в файле FILE_USER_INIT_DATA, по его имени в файле.
 * 
 * @param name Имя пользователя в файле
 * @return Структура с даными пользователя
 */
ass::InitData getInitDataFromName(std::string name);
} // namespace ass

#include "Resource.hpp"

#endif //FITNESSASSISTANT_HPP