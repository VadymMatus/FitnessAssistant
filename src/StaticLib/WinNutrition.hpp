/* -*- C++ -*- */
#ifndef GTKMM_WINNUTRITION_HPP
#define GTKMM_WINNUTRITION_HPP

#include "FitnessAssistant.hpp"
#include <ctime>
#include <iomanip>
#include <math.h>

class WinNutrition
{
  public:
    WinNutrition(int nUserInit, bool isRoot);
    ~WinNutrition();

    /***
   * @brief Get the Scrolled Window object
   * 
   * @return Gtk::ScrolledWindow& 
   */
    Gtk::ScrolledWindow *getScrolledWindow();

  private:
    typedef Gtk::TreeModel::Children type_children;

    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
      public:
        ModelColumns() { add(m_col_name); }

        Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    } m_Columns;

    // модель таблицы дневника
    class ModelColumnsDiaries : public Gtk::TreeModel::ColumnRecord
    {
      public:
        ModelColumnsDiaries()
        {
            add(m_col_id);
            add(m_col_name);
            add(m_calories);
            add(m_carbon);
            add(m_fats);
            add(m_proteins);
            add(m_weigth);
        }

        Gtk::TreeModelColumn<size_t> m_col_id;
        Gtk::TreeModelColumn<Glib::ustring> m_col_name;
        Gtk::TreeModelColumn<Glib::ustring> m_calories;
        Gtk::TreeModelColumn<Glib::ustring> m_carbon;
        Gtk::TreeModelColumn<Glib::ustring> m_fats;
        Gtk::TreeModelColumn<Glib::ustring> m_proteins;
        Gtk::TreeModelColumn<Glib::ustring> m_weigth;
    } m_Columns_Diaries;

    /***
   * @brief Вывод окна для редактирования записи.
   *  
   */
    void show_window_add_diaries();

    /***
   * @brief Удаляет выделеную запись из таблицы и 
   *    файла FILE_USER_STATISTIC_DATA_JSON
   * 
   */
    void on_button_remove();

    /***
   * @brief Добавления записи в таблицу и файл
   *    после того как пользователь заполнил форму
   *    в окне добавления записи
   * 
   */
    void on_button_add_diaries();

    /***
   * @brief Отмена доббавления новой записи,
   *    Скрывает окно добавления новой записи
   * 
   */
    void on_button_cancel_diaries();

    /***
   * @brief Показывает окно с формой для 
   *    добавления новой записи,
   * 
   */
    void show_window_edit_diaries();

    /***
   * @brief Редактирование выделиной записи или 
   *    первую по умолчанию. 
   * После заполнения формы в окне редактирования 
   * запис, запись обновляеться в таблице и 
   * в файле FILE_USER_STATISTIC_DATA_JSON 
   */
    void on_button_edit_diaries();

    /***
   * @brief Отмена редактирования записи.
   * Скрытие окна редактирования записи.
   */
    void on_button_cancel_edit_diaries();

    /***
   * @brief Заполняет структуру даными из файла.
   * 
   * Считуеться статистика пользователя с сегоднешней датой,
   * или создаёться новая записиь с сегоднешней датой в файле
   * FILE_USER_STATISTIC_DATA_JSON
   */
    void fill_struct_calculData();

    /***
   * @brief Get the date object
   * 
   * @param format Visit http://www.cplusplus.com/reference/ctime/strftime/ 
   * @return std::string Date 
   */
    const std::string get_date(const char *format = "%d-%m-%Y");

    /***
   * Функция-обработчик on_button_icon_find(), вызываеться при нажатии на
   * иконку поиска, считует имя из поля m_Entery_Account, сравнивает с всеми
   * зарезервироваными пользователями в системе и выводи все данные о
   * пользователе. 
   */
    void on_button_icon_find(Gtk::EntryIconPosition, const GdkEventButton *);

    /***
   * Функция-обработчик on_button_enter_find(), вызываеться при нажатии на
   * <Enter> в поле m_Entery_Account, считует имя из поля m_Entery_Account,
   * сравнивает с всеми зарезервироваными пользователями в системе и 
   * выводи все данные о пользователе. 
   */
    void on_button_enter_find();

    /***
   * Функция set_list_user(), считывает имена всех зарезервированых 
   * пользователей из файла FILE_USER_INIT_DATA и добавляет их в подсказку,
   * которая будет отображаться при поиске.
   */
    void set_list_user();

    /***
   * @brief Функция поиска записи пользователя.
   * 
   * Посик записи пользователя по его имени в файле 
   * FILE_USER_INIT_DATA.
   */
    void find_user();

    /***
   * @brief Заполнение структуры даных о пользователе. 
   * 
   * После выполнения функции посика пользователя в файле
   * FILE_USER_INIT_DATA, стаёт известна позиция записи 
   * пользователя в файле, она храниться в переменной 
   * nUserInit. После чего заполняеться структура initData.
   */
    void fill_struct_initData();

    /***
   * @brief Функция сохранения всех изменений в файл
   *    FILE_USER_STATISTIC_DATA_JSON.
   * 
   * Функция вызываеться в деструкторе.   
   * 
   */
    void save_data();

    /***
   * @brief Функция-обработчик, прибавляет 1 литр
   *    к общей сумме употреблёной воды пользователем.
   */
    void on_button_add_1();

    /***
   * @brief Функция-обработчик, прибавляет 0.5 литра
   *    к общей сумме употреблёной воды пользователем.
   */
    void on_button_add_0_5();

    /***
   * @brief Функция-обработчик, прибавляет 0.2 литра
   *    к общей сумме употреблёной воды пользователем.
   */
    void on_button_add_0_2();

    /***
   * @brief Функция-обработчик, отнимает 1 литр
   *    от общей суммы употреблёной воды пользователем.
   */
    void on_button_minus_1();

    /***
   * @brief Функция-обработчик, отнимает 0.5 литра
   *    от общей суммы употреблёной воды пользователем.
   */
    void on_button_minus_0_5();

    /***
   * @brief Функция-обработчик, отнимает 0.2 литра
   *    от общей суммы употреблёной воды пользователем.
   */
    void on_button_minus_0_2();

    // TODO: Задокументировать
    void on_selection_changed();

    // TODO: Задокументировать
void fill_table();


    ass::CalculData calculData; // расчитаные даные пользователя
    ass::InitData initData;     // даные о пользователе

    std::vector<ass::Note> note;
    std::vector<int> exception;
    std::map<Gtk::Button, int> mp;
    std::stringstream ss, need_water;
    std::string today_date;

    Json new_user_json, new_date_json, m_file_statistic_json,
        user_json, date_json, diaries_records_json;

    bool isRoot;
    bool isStarted_Edit_Diaries = true;
    bool isStarted = true;

    size_t count = 0;
    int nUserInit;
    int nUserStat;

    /* Окно добавления */
    Gtk::Window window;
    Gtk::HBox m_HBox;
    Gtk::VBox m_VBox_Main_Diaries, m_VBox_Diaries_Record;
    Gtk::Frame m_frame_name, m_frame_protein, m_frame_fat, m_frame_carbon, m_frame_calories;
    Gtk::Entry m_entry_name, m_entry_protein, m_entry_fat, m_entry_carbon, m_entry_calories;
    Gtk::ButtonBox m_butonbox_diaries;
    Gtk::Button m_button_add_diaries, m_button_cancel_diaries;

    /* Окно редактирования */
    Gtk::Window window_edit_diaries;
    Gtk::HBox m_HBox_Edit_Diaries;
    Gtk::VBox m_VBox_Main_Edit_Diaries, m_VBox_Diaries_Edit_Diaries;
    Gtk::Frame m_frame_name_edit_diaries, m_frame_protein_edit_diaries,
        m_frame_fat_edit_diaries, m_frame_carbon_edit_diaries, m_frame_calories_edit_diaries;
    Gtk::Entry m_entry_name_edit_diaries, m_entry_protein_edit_diaries,
        m_entry_fat_edit_diaries, m_entry_carbon_edit_diaries, m_entry_calories_edit_diaries;
    Gtk::ButtonBox m_butonbox_edit_diaries;
    Gtk::Button m_button_edit_diaries, m_button_cancel_edit_diaries;

    /* Расчет калорий */
    Gtk::ScrolledWindow m_ScrolledWindow_Main;
    Gtk::Frame m_frame_celcul_cal;
    Gtk::HBox m_HBox_Calcul_Cal, m_HBox_BMR, m_HBox_Nutrition, m_HBox_Target, m_HBox_Finish;
    Gtk::VBox m_VBox_Find,
        m_VBox_Main,
        m_VBox_Calcul_Cal;
    Gtk::Label m_label_find,
        m_label_BMR, m_label_BMR_data,
        m_label_nutrition, m_label_nutrition_data,
        m_label_target, m_label_target_data,
        m_label_need;
    Gtk::Entry m_entry_find;

    /* Учет жидкости */
    Gtk::Frame m_frame_water;
    Gtk::HBox m_HBox_Water;
    Gtk::VBox m_VBox_Bottle_1, m_VBox_Bottle_0_5, m_VBox_Glass_0_2, m_VBox_Result_Water;
    Gtk::Button m_button_add_1, m_button_add_0_5, m_button_add_0_2,
        m_button_minus_1, m_button_minus_0_5, m_button_minus_0_2;
    Gtk::Image bottle_1, bottle_0_5, glass_0_2;
    Gtk::Label m_label_result_water;

    /* Дневник */
    Gtk::Frame m_frame_diaries;
    Gtk::VBox m_VBox_Diaries;
    Gtk::ButtonBox m_butonbox;
    Gtk::Button m_button_add, m_button_remove, m_button_edit;
    Gtk::TreeView m_TreeView;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
    Glib::RefPtr<Gtk::TreeSelection> m_refTreeSelection;
    Gtk::TreeModel::Row row;
};

#endif // GTKMM_WINNUTRITION_HPP