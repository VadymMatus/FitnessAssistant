#include "WinInitData.hpp"

WinInitData::WinInitData(int n_usr, bool is_root, bool newusr)
    : nUserInit(n_usr), isRoot(is_root), _new_user(newusr),
      m_button_quit(resources.close),
      m_button_save(resources.save),
      m_button_del_usr(resources.del),
      m_ButtonBox(Gtk::ORIENTATION_HORIZONTAL),
      m_HBox_Account(Gtk::ORIENTATION_VERTICAL),
      m_HBox_Sex_Target_Activity(Gtk::ORIENTATION_HORIZONTAL, 5),
      m_HBox_Weight_And_Growth(Gtk::ORIENTATION_HORIZONTAL, 5),
      m_HBox_Name_Age(Gtk::ORIENTATION_HORIZONTAL, 5),
      m_VBox_Passwd(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Main(Gtk::ORIENTATION_VERTICAL, 20),
      m_VBox_Name(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Age(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Current_Weight(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Desired_Weight(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Growth(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Sex(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Target(Gtk::ORIENTATION_VERTICAL),
      m_VBox_Activity(Gtk::ORIENTATION_VERTICAL),
      m_HBox_Visible_Label(Gtk::ORIENTATION_HORIZONTAL),
      m_Label_Name(resources.name),
      m_Label_Age(resources.age + ":", Gtk::ALIGN_START),
      m_Label_Current_Weight(resources.current_weight + ":", Gtk::ALIGN_START),
      m_Label_Desired_Weight(resources.desired_weight + ":", Gtk::ALIGN_START),
      m_Label_Growth(resources.current_growth + ":", Gtk::ALIGN_START),
      m_Label_Sex(resources.sex + ":"),
      m_Label_Activity(resources.activity),
      m_Label_Target(resources.target),
      m_Label_Account(resources.enter_name_user_for_edit),
      m_Label_Passwd(resources.new_user_passwd),
      m_adjustment_age(Gtk::Adjustment::create(PUserInitData->Age, 10.0, 120.0, 1.0, 5.0, 0.0)),
      m_Adjustment_Current_Weight(Gtk::Adjustment::create(PUserInitData->CurrentWeight, 20.0, 200.0, 1.0, 1.0, 0.0)),
      m_Adjustment_Desired_Weight(Gtk::Adjustment::create(PUserInitData->DesiredWeight, 20.0, 200.0, 1.0, 1.0, 0.0)),
      m_Adjustment_Growth(Gtk::Adjustment::create(PUserInitData->Growth, 100.0, 300.0, 1.0, 1.0, 0.0)),
      m_SpinButton_Age(m_adjustment_age),
      m_SpinButton_Current_Weight(m_Adjustment_Current_Weight),
      m_SpinButton_Desired_Weight(m_Adjustment_Desired_Weight),
      m_SpinButton_Growth(m_Adjustment_Growth),
      m_CheckButton_Visible(resources.show)
{
    /********** Параметры окна ***********/
    this->set_position(Gtk::WIN_POS_CENTER);
    this->set_title((_new_user == true ? resources.created_profile : resources.edit_profile));
    this->set_border_width(3);
    this->set_resizable(false);

    this->add(m_VBox_Main);

    m_VBox_Main.set_border_width(5);
    /*============================================*\
                    Учётная запись
    \*============================================*/

    if (_new_user == false and isRoot == true)
    {
        m_VBox_Main.pack_start(m_HBox_Account, Gtk::PACK_SHRINK);

        m_Entry_Account.set_max_length(55);

        m_Entry_Account.set_icon_from_icon_name("edit-find-replace", Gtk::ENTRY_ICON_SECONDARY);

        m_Entry_Account.signal_activate().connect(sigc::mem_fun(*this, &WinInitData::on_button_enter_find));
        m_Entry_Account.signal_icon_press().connect(sigc::mem_fun(*this, &WinInitData::on_button_icon_find));

        set_list_user(); // создаю список пользователей

        m_HBox_Account.pack_start(m_Entry_Account, Gtk::PACK_SHRINK);
        m_HBox_Account.pack_start(m_Label_Account);
    }
    // WinInitData::set_usr_init_data();

    /*============================================*\
                    Имя и возраст
    \*============================================*/
    m_VBox_Main.pack_start(m_HBox_Name_Age);

    /******************* Имя ********************/
    m_VBox_Name.pack_start(m_Label_Name);

    m_Entry_Name.set_max_length(55);
    if (_new_user == false and isRoot == true)
        m_Entry_Name.set_sensitive(false);
    m_VBox_Name.pack_start(m_Entry_Name);

    m_HBox_Name_Age.pack_start(m_VBox_Name, Gtk::PACK_EXPAND_WIDGET, 5);

    /******************* Возраст ********************/
    m_VBox_Age.pack_start(m_Label_Age);
    m_VBox_Age.pack_start(m_SpinButton_Age);
    m_SpinButton_Age.set_wrap();
    if (_new_user == false and isRoot == true)
        m_SpinButton_Age.set_sensitive(false);

    m_HBox_Name_Age.pack_start(m_VBox_Age, Gtk::PACK_EXPAND_WIDGET, 5);

    /*============================================*\
                        Вес и Рост
    \*============================================*/

    m_VBox_Main.pack_start(m_HBox_Weight_And_Growth);

    /****************** Текущий вес **********************/
    m_VBox_Current_Weight.pack_start(m_Label_Current_Weight);

    m_VBox_Current_Weight.pack_start(m_SpinButton_Current_Weight);
    m_SpinButton_Current_Weight.set_wrap();
    if (_new_user == false and isRoot == true)
        m_SpinButton_Current_Weight.set_sensitive(false);

    m_HBox_Weight_And_Growth.pack_start(m_VBox_Current_Weight, Gtk::PACK_EXPAND_WIDGET, 5);

    /****************** Желаемый вес **********************/
    m_VBox_Desired_Weight.pack_start(m_Label_Desired_Weight);

    m_VBox_Desired_Weight.pack_start(m_SpinButton_Desired_Weight);
    m_SpinButton_Desired_Weight.set_wrap();
    if (_new_user == false and isRoot == true)
        m_SpinButton_Desired_Weight.set_sensitive(false);

    m_HBox_Weight_And_Growth.pack_start(m_VBox_Desired_Weight, Gtk::PACK_EXPAND_WIDGET, 5);

    /****************** Рост **********************/
    m_VBox_Growth.pack_start(m_Label_Growth);

    m_VBox_Growth.pack_start(m_SpinButton_Growth);
    m_SpinButton_Growth.set_wrap();
    if (_new_user == false and isRoot == true)
        m_SpinButton_Growth.set_sensitive(false);

    m_HBox_Weight_And_Growth.pack_start(m_VBox_Growth, Gtk::PACK_EXPAND_WIDGET, 5);

    /*============================================*\
                 Пол, активность, цель
    \*============================================*/
    m_VBox_Main.pack_start(m_HBox_Sex_Target_Activity);

    /****************** Пол **********************/
    m_VBox_Sex.pack_start(m_Label_Sex);

    m_VBox_Sex.pack_start(m_Combo_Sex);
    m_Combo_Sex.append(resources.male);
    m_Combo_Sex.append(resources.female);
    if (_new_user == false and isRoot == true)
        m_Combo_Sex.set_sensitive(false);

    m_HBox_Sex_Target_Activity.pack_start(m_VBox_Sex);

    /****************** Активность **********************/
    m_VBox_Activity.pack_start(m_Label_Activity);

    m_VBox_Activity.pack_start(m_Combo_Activity);
    m_Combo_Activity.append(resources.activity_sit_down);
    m_Combo_Activity.append(resources.activity_normaly);
    m_Combo_Activity.append(resources.activity_hard);

    if (_new_user == false and isRoot == true)
        m_Combo_Activity.set_sensitive(false);

    m_HBox_Sex_Target_Activity.pack_start(m_VBox_Activity);

    /****************** Цель **********************/
    m_VBox_Target.pack_start(m_Label_Target);

    m_VBox_Target.pack_start(m_Combo_Target);
    m_Combo_Target.append(resources.lose_weight);
    m_Combo_Target.append(resources.support_weight);
    m_Combo_Target.append(resources.fast);

    if (_new_user == false and isRoot == true)
        m_Combo_Target.set_sensitive(false);

    m_HBox_Sex_Target_Activity.pack_start(m_VBox_Target);
    /*============================================*\
                        Пароль
    \*============================================*/
    m_VBox_Passwd.pack_start(m_Entry_Passwd, Gtk::PACK_SHRINK);
    m_Entry_Passwd.set_max_length(55);
    m_Entry_Passwd.set_text("");
    m_Entry_Passwd.set_visibility(false);

    m_CheckButton_Visible.signal_toggled().connect(sigc::mem_fun(*this, &WinInitData::on_checkbox_visibility));

    m_HBox_Visible_Label.pack_start(m_CheckButton_Visible);
    m_HBox_Visible_Label.pack_start(m_Label_Passwd);

    if (_new_user == false and isRoot == true)
    {
        m_Label_Passwd.set_text(resources.passwd_user);
        m_CheckButton_Visible.set_sensitive(false);
    }

    m_VBox_Passwd.pack_start(m_HBox_Visible_Label);

    m_VBox_Main.pack_start(m_VBox_Passwd);

    /*============================================*\
                      Нижний бокс
    \*============================================*/
    m_button_quit.signal_clicked().connect(sigc::mem_fun(*this, &WinInitData::on_button_quit));
    m_button_del_usr.signal_clicked().connect(sigc::mem_fun(*this, &WinInitData::on_button_del_usr));
    m_button_save.signal_clicked().connect(sigc::mem_fun(*this, &WinInitData::on_button_save));

    if (_new_user == false and isRoot == true)
    {
        m_button_del_usr.set_sensitive(false);
        m_button_save.set_sensitive(false);
    }

    m_ButtonBox.pack_start(m_button_quit, Gtk::PACK_EXPAND_WIDGET, 0);
    if (_new_user == false)
        m_ButtonBox.pack_start(m_button_del_usr, Gtk::PACK_EXPAND_WIDGET, 0);
    m_ButtonBox.pack_start(m_button_save, Gtk::PACK_EXPAND_WIDGET, 0);

    m_VBox_Main.pack_start(m_ButtonBox, Gtk::PACK_EXPAND_WIDGET, 0);

    if (isRoot == false)
        WinInitData::set_usr_init_data();

    this->show_all_children();
}

/*===================================================*\
            СОХРАНЕНИЕ ИЗМЕНЕНИЙ В ФАЙЛ
\*===================================================*/
void WinInitData::on_button_save()
{
    /********** Контроль имени *********/
    size_t index = m_Entry_Name.get_text().find_first_of("=-+*/.|\\\",;'[]`!@#$%^&()_<>:?{}№?");
    if (index != std::string::npos)
    {
        m_Label_Name.set_text(resources.invalid_char + " \"");
        m_Label_Name.set_text(m_Label_Name.get_text() + m_Entry_Name.get_text()[index] + "\" " + resources.in_name + "!");
        m_Label_Name.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
        return;
    }
    else if (m_Entry_Name.get_text().size() == 0)
    {
        m_Label_Name.set_text(resources.line_should_filled + "!");
        m_Label_Name.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
        return;
    }
    else
    {
        if (strcmp(PUserInitData->Name, m_Entry_Name.get_text().c_str()) != 0)
        {
            ass::InitData temp;
            std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
            ifs.seekg(sizeof(ass::root), std::ios_base::beg);

            size_t size = sizeof(ass::InitData);

            while (ifs.read((char *)&temp, size))
                if (strcmp(temp.Name, m_Entry_Name.get_text().c_str()) == 0)
                {
                    m_Label_Name.set_text(resources.name_exist + "!");
                    m_Label_Name.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
                    return;
                }
        }

        if (m_Entry_Name.get_text().size() == 0)
        {
            m_Label_Name.set_text(resources.line_should_filled + "!");
            m_Label_Name.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
            return;
        }

        m_Label_Name.set_text(resources.name);
        m_Label_Name.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
    }

    /********** Контроль пароля *********/
    index = m_Entry_Passwd.get_text().find_first_of("=-+*/.|\\\",;'[]`!@#$%^&()_<>:?{}№?");
    if (index != std::string::npos)
    {
        m_Label_Passwd.set_text(resources.invalid_char + " \"");
        m_Label_Passwd.set_text(m_Label_Passwd.get_text() + m_Entry_Passwd.get_text()[index] + "\" " + resources.in_line + "!");
        m_Label_Passwd.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
        return;
    }
    else if (m_Entry_Passwd.get_text().size() == 0)
    {
        m_Label_Passwd.set_text(resources.line_should_filled);
        m_Label_Passwd.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
        return;
    }
    else
    {
        if (_new_user == false)
            m_Label_Passwd.set_text(resources.passwd_user);
        else
            m_Label_Passwd.set_text(resources.new_user_passwd);

        m_Label_Passwd.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
    }

    /********** Заполнение структуры данными **********/
    std::string str = m_Entry_Name.get_text().c_str();
    std::copy(str.begin(), str.end(), PUserInitData->Name);
    PUserInitData->Name[str.size()] = '\0';

    /********* Удаление из имени лишних пробелов *********/
    for (size_t i(0); i < strlen(PUserInitData->Name); i++)
        if (PUserInitData->Name[i] == ' ' and
                (PUserInitData->Name[i + 1] == ' ' or
                 PUserInitData->Name[i + 1] == '\0') or
            PUserInitData->Name[0] == ' ')
        {
            for (size_t j(i); j < strlen(PUserInitData->Name); j++)
                PUserInitData->Name[j] = PUserInitData->Name[j + 1];
            i -= 1;
        }

    // /********* Первые буквы заглавные *********/
    // PUserInitData->Name[0] = toupper(PUserInitData->Name[0]);
    // for (size_t i = 1; i < strlen(PUserInitData->Name); i++)
    //     if (PUserInitData->Name[i] == ' ' and PUserInitData->Name[i + 1] != ' ')
    //         PUserInitData->Name[i + 1] = toupper(PUserInitData->Name[i + 1]);

    PUserInitData->Age = atoi(m_SpinButton_Age.get_text().c_str());
    PUserInitData->CurrentWeight = atoi(m_SpinButton_Current_Weight.get_text().c_str());
    PUserInitData->DesiredWeight = atoi(m_SpinButton_Desired_Weight.get_text().c_str());
    PUserInitData->Growth = atoi(m_SpinButton_Growth.get_text().c_str());
    // TODO: Заменить оператор стравнивания на встроеные методы стравнивания
    PUserInitData->Sex = (m_Combo_Sex.get_active_text() == resources.male);
    PUserInitData->Activity = (m_Combo_Activity.get_active_text() == resources.activity_sit_down
                                   ? ass::ActivitySedentary
                                   : m_Combo_Activity.get_active_text() == resources.activity_normaly
                                         ? ass::ActivityNormal
                                         : ass::ActivityHigh);
    PUserInitData->Target = (m_Combo_Target.get_active_text() == resources.lose_weight
                                 ? ass::TargetLoseWeight
                                 : m_Combo_Target.get_active_text() == resources.support_weight
                                       ? ass::TargetSupport
                                       : ass::TargetGetFat);

    str = m_Entry_Passwd.get_text().c_str();
    std::copy(str.begin(), str.end(), PUserInitData->Passwd);
    PUserInitData->Passwd[str.size()] = '\0';

    /************* Запись структуры в файл *************/
    if (!(ass::FileExists(FILE_USER_INIT_DATA)))
    {
        ass::root *root = new ass::root;

        std::cout << "Файл \"" << FILE_USER_INIT_DATA << "\" не существует, создаёться новый … ";
        system("mkdir -p " FOLDER_USR " " FOLDER_USR_DATA);
        std::ofstream ofs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::out);
        std::cout << (ofs.is_open() ? "готово" : "\033[1;31mошибка\033[0m") << std::endl
                  << "Создание учётной записи администратора … "
                  << (ofs.write((char *)root, sizeof(ass::root)) ? "готово" : "\033[1;31mошибка\033[0m")
                  << std::endl;

        ofs.close();
        delete root;
    }

    std::fstream fs(FILE_USER_INIT_DATA, std::ios_base::binary | (_new_user == true ? std::ios_base::app : std::ios_base::out | std::ios_base::in));

    if (_new_user == false)
        fs.seekp(nUserInit * sizeof(ass::InitData) + sizeof(ass::root), std::ios_base::beg);

    std::cout << "Запись изменений в файл \"" << FILE_USER_INIT_DATA << "\" … "
              << (fs.write((char *)PUserInitData, sizeof(*PUserInitData)) ? "готово" : "\033[1;31mошибка\033[0m")
              << std::endl;

    fs.close();

    CalculData calculData(*PUserInitData);

    this->hide();
}

void WinInitData::on_button_quit()
{
    hide();
}

/*===================================================*\
          ЗАПИСЬ ДАННЫХ ИЗ ФАЙЛА В СТРУКТУРУ
\*===================================================*/
void WinInitData::set_usr_init_data()
{
    if (ass::FileExists(FILE_USER_INIT_DATA))
        if (!ass::FileEmpty(FILE_USER_INIT_DATA))
        {
            std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);

            if (ifs.is_open())
            {
                ifs.seekg(nUserInit * sizeof(ass::InitData) + sizeof(ass::root), std::ios_base::beg);
                ifs.read((char *)PUserInitData, sizeof(*PUserInitData));
                ifs.close();
            }
        }

    std::stringstream ss;
    ss << PUserInitData->Name;
    m_Entry_Name.set_text(ss.str());
    ss.str("");

    ss << PUserInitData->Passwd;
    m_Entry_Passwd.set_text(ss.str());
    ss.str("");

    ss << PUserInitData->Age;
    m_SpinButton_Age.set_text(ss.str());
    ss.str("");

    ss << PUserInitData->CurrentWeight;
    m_SpinButton_Current_Weight.set_text(ss.str());
    ss.str("");
    ss << PUserInitData->DesiredWeight;

    m_SpinButton_Desired_Weight.set_text(ss.str());
    ss.str("");

    ss << PUserInitData->Growth;
    m_SpinButton_Growth.set_text(ss.str());
    ss.str("");

    m_Combo_Sex.set_active((PUserInitData->Sex ? 0 : 1));
    m_Combo_Activity.set_active((PUserInitData->Activity == ass::ActivitySedentary
                                     ? 0
                                     : PUserInitData->Activity == ass::ActivityNormal ? 1 : 2));

    m_Combo_Target.set_active((PUserInitData->Target == ass::TargetLoseWeight
                                   ? 0
                                   : PUserInitData->Target == ass::TargetSupport ? 1 : 2));
}

WinInitData::~WinInitData()
{
    std::cout << "Завершился модуль WinInitData" << std::endl;
    delete PUserInitData;
}

void WinInitData::on_button_icon_find(Gtk::EntryIconPosition, const GdkEventButton *)
{
    ass::InitData *temp = new ass::InitData;
    size_t size = sizeof(ass::InitData);
    size_t index(0);

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)temp, size))
        if (strcmp(temp->Name, m_Entry_Account.get_text().c_str()) == 0)
        {
            nUserInit = index;
            break;
        }
        else
            index++;

    ifs.close();

    m_Label_Account.set_text((nUserInit == -1 ? resources.user_not_found : resources.enter_name_user_for_edit));
    m_Label_Account.override_color(Gdk::RGBA((nUserInit == -1 ? "yellow" : "white")), Gtk::STATE_FLAG_NORMAL);

    m_Entry_Name.set_sensitive(nUserInit != -1);
    m_SpinButton_Age.set_sensitive(nUserInit != -1);
    m_SpinButton_Current_Weight.set_sensitive(nUserInit != -1);
    m_SpinButton_Desired_Weight.set_sensitive(nUserInit != -1);
    m_SpinButton_Growth.set_sensitive(nUserInit != -1);
    m_Combo_Sex.set_sensitive(nUserInit != -1);
    m_Combo_Activity.set_sensitive(nUserInit != -1);
    m_Combo_Target.set_sensitive(nUserInit != -1);
    m_Combo_Sex.set_sensitive(nUserInit != -1);
    m_button_save.set_sensitive(nUserInit != -1);
    m_button_del_usr.set_sensitive(nUserInit != -1);
    m_Entry_Passwd.set_sensitive(nUserInit != -1);
    m_CheckButton_Visible.set_sensitive(nUserInit != -1);

    if (_new_user == false)
        WinInitData::set_usr_init_data();

    delete temp;
}

void WinInitData::on_button_enter_find()
{
    ass::InitData *temp = new ass::InitData;
    size_t size = sizeof(ass::InitData);
    size_t index(0);

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)temp, size))
        if (strcmp(temp->Name, m_Entry_Account.get_text().c_str()) == 0)
        {
            nUserInit = index;
            break;
        }
        else
            index++;

    ifs.close();

    m_Label_Account.set_text((nUserInit == -1 ? resources.user_not_found : resources.enter_name_user_for_edit));
    m_Label_Account.override_color(Gdk::RGBA((nUserInit == -1 ? "yellow" : "white")), Gtk::STATE_FLAG_NORMAL);

    m_Entry_Name.set_sensitive(nUserInit != -1);
    m_SpinButton_Age.set_sensitive(nUserInit != -1);
    m_SpinButton_Current_Weight.set_sensitive(nUserInit != -1);
    m_SpinButton_Desired_Weight.set_sensitive(nUserInit != -1);
    m_SpinButton_Growth.set_sensitive(nUserInit != -1);
    m_Combo_Sex.set_sensitive(nUserInit != -1);
    m_Combo_Activity.set_sensitive(nUserInit != -1);
    m_Combo_Target.set_sensitive(nUserInit != -1);
    m_Combo_Sex.set_sensitive(nUserInit != -1);
    m_button_save.set_sensitive(nUserInit != -1);
    m_button_del_usr.set_sensitive(nUserInit != -1);
    m_Entry_Passwd.set_sensitive(nUserInit != -1);
    m_CheckButton_Visible.set_sensitive(nUserInit != -1);

    if (_new_user == false)
        WinInitData::set_usr_init_data();

    delete temp;
}

void WinInitData::set_list_user()
{
    ass::InitData *temp = new ass::InitData;

    size_t size = sizeof(ass::InitData);
    auto completion = Gtk::EntryCompletion::create();
    m_Entry_Account.set_completion(completion);

    auto refCompletionModel = Gtk::ListStore::create(m_Columns);
    completion->set_model(refCompletionModel);

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);

    Gtk::TreeModel::Row row = *(refCompletionModel->append());
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);
    while (ifs.read((char *)temp, size))
    {
        row = *(refCompletionModel->append());
        row[m_Columns.m_col_name] = temp->Name;
    }

    ifs.close();

    completion->set_text_column(m_Columns.m_col_name);

    delete temp;
}

void WinInitData::on_checkbox_visibility()
{
    m_Entry_Passwd.set_visibility(m_CheckButton_Visible.get_active());
}

void WinInitData::on_button_del_usr()
{
    int64_t count(0);
    ass::root root;
    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::ate);
    count = ((int64_t)ifs.tellg()) / sizeof(ass::InitData);
    ifs.seekg(0, std::ios_base::beg);

    ass::InitData temp[count];

    ifs.read((char *)&root, sizeof(root));
    ifs.read(reinterpret_cast<char *>(temp), count * sizeof(temp[0]));
    ifs.close();

    for (int i(nUserInit); i < count; i++)
        temp[i] = temp[i + 1];

    Gtk::MessageDialog dialog(*this, resources.del_user, false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_OK_CANCEL);
    if (dialog.run() == Gtk::RESPONSE_OK)
    {
        std::ofstream ofs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::trunc);

        ofs.write((char *)&root, sizeof(root));
        ofs.write(reinterpret_cast<char *>(temp), (count - 1) * sizeof(temp[0]));
        ofs.close();

        Json json;
        // удаление статистики
        ifs.open(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
        ifs >> json;
        ifs.close();

        json.erase(json.find(PUserInitData->Name));
        ofs.open(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
        ofs << json.dump(SPACE_IN_JSON_FILES);
        ofs.close();

        // удаление расчитаных данных
        ifs.open(FILE_USER_CALCUL_DATA_JSON, std::ios_base::binary);
        ifs >> json;
        ifs.close();

        json.erase(json.find(PUserInitData->Name));
        ofs.open(FILE_USER_CALCUL_DATA_JSON, std::ios_base::binary);
        ofs << json.dump(SPACE_IN_JSON_FILES);
        ofs.close();

        WinInitData::on_button_quit();
    }

    // ass::InitData *temp = new ass::InitData;
    // std::fstream fs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::out | std::ios_base::in);
    // fs.seekp(nUserInit * sizeof(*temp) + sizeof(*temp) + sizeof(ass::root), std::ios_base::beg);
    // while (fs.read((char *)temp, sizeof(*temp)))
    // {
    //     fs.seekp(sizeof(*temp) * (-2), std::ios_base::cur);
    //     fs.write((char *)temp, sizeof(*temp));
    //     fs.seekp(sizeof(*temp), std::ios_base::cur);
    // }
    // // fs.read((char *)temp, sizeof(*temp));
    // fs.seekp(sizeof(*temp) * (-1), std::ios_base::cur);
    // fs.put(-1);
    // // fs.write(EOF, sizeof(*temp));

    // fs.close();
}