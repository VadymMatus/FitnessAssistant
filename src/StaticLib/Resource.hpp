#ifndef RESOURCE_HPP
#define RESOURCE_HPP

#include "FitnessAssistant.hpp"
#include "json.hpp"

/***
 * @brief Файл локализации приложения.
 *  Структура считует текущие настройки из файла FILE_PROG_PREFERENCES,
 *  после считует константу которая указывает на язык интерфейса приложения.
 *  Зная язык интерфейса открываеться соотвецтвующий языковый пакет ресурсов
 *  из папки FOLDER_PROG_LANGUAGE и иницыализируються все переменые этой 
 *  структуры, после чего можна использовать ресурсы через объект resources.
 * 
 */
struct Resource
{
    Resource();

    std::string exit, comin, registry, user_record, user_passwd,
        login, error, invalid_name_or_passwd, close, save, del,
        name, age, current_weight, desired_weight, current_growth,
        sex, activity, target, enter_name_user_for_edit, new_user_passwd,
        show, created_profile, edit_profile, male, female, activity_sit_down,
        activity_normaly, activity_hard, lose_weight, support_weight, fast,
        passwd_user, invalid_char, in_name, line_should_filled, name_exist,
        in_line, user_not_found, title_main, trening, tab_food, statistic,
        setting, init_data, new_user, sorted, filter, percentage_ratio, main,
        users, inquiry_target, inquiry_activity, inquiry_sex, sort_activity,
        sort_sit_down, sort_pass, A_z, Z_a, sort_weight_down, sort_weight_up,
        sort_desired_weight_down, sort_desired_weight_up, sort_age_down,
        sort_age_up, sort_growth_down, sort_growth_up, number, growth,
        result, percentage_ratio_target, need_lose_weight, need_support_weight,
        need_fast, percentage_ratio_activity, loos_people, hard_people,
        percentage_ratio_sex, percentage_ratio_male, percentage_ratio_female,
        calcul_calories, uchet_water, diaries_nutrition, find_user, BMR, nutrition,
        result_nutrition, need, summ, recomendation_you, p, f, c, add, plus, minus,
        name_dish, protein_in_100g, fat_in_100g, carbon_in_100g, calories_in_100g,
        back, recomendation, Kkal, burn, type, litr, name_mean_food, cancel,
        msg_record_not_found, edvances_check_name, title_diaries_addeding,
        title_get_the_uset, admin, week, month, all_time, UPS, user_with_name_not_found,
        you_not_have_statistic, date, calories, fats, carbons, proteins, water, edit,
        name_exercise, short_name_exercise, type_exercise, title_addrecord,
        exercise_name_exist, empty_line, enter_type_exercise,
        enter_name_exercise, title_deleterecord, delete_record, warning_delete_record,weigth,
        find_clear, edit_exercise, exercise_exist, find, change_language, del_user;
        
} const resources;

#endif // RESOURCE_HPP