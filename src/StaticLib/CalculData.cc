#include "CalculData.hpp"

CalculData::CalculData(const ass::InitData &initData)
    : initData(initData)
{
    std::cout << "I: Старт модуля CalculData >>>" << '\n';

    read_file_calcul_data();

    std::cout << "I: Расчет дневной нормы воды, калорий, жиров, углеводы … ";
    strcpy(calculData.Name, initData.Name);
    calculData.Water = CalculWater();
    calculData.Calories = CalculCalories();
    (initData.Target == ass::TargetLoseWeight ? calculData.Calories -= 300 : initData.Target == ass::TargetGetFat ? calculData.Calories += 300 : calculData.Calories += 0);
    calculData.Fats = CalculFats();
    calculData.Carbon = CalculCarbon();
    calculData.Proteins = CalculProteins();
    std::cout << "готово" << std::endl;

    std::cout << "I: Новые росчитаные даные:" << std::endl
              << "Имя: " << initData.Name << std::endl
              << "Калории: " << calculData.Calories << " Ккал" << std::endl
              << "Угдеводы: " << calculData.Carbon << " г." << std::endl
              << "Белки: " << calculData.Proteins << " г." << std::endl
              << "Жири: " << calculData.Fats << " г." << std::endl
              << "Вода: " << calculData.Water << " л." << std::endl;

    std::cout << "I: Запись изменений в файл " << FILE_USER_CALCUL_DATA_JSON << " … ";
    Json update = Json::object(); // обновлёная запись
    update["calories"] = calculData.Calories;
    update["carbon"] = calculData.Carbon;
    update["proteins"] = calculData.Proteins;
    update["fats"] = calculData.Fats;
    update["water"] = calculData.Water;

    m_file_calcul_data_json[calculData.Name] = update;

    std::ofstream ofs(FILE_USER_CALCUL_DATA_JSON, std::ios_base::binary);
    ofs << m_file_calcul_data_json.dump(SPACE_IN_JSON_FILES);
    ofs.close();

    std::cout << (ofs.is_open() == false ? "готово" : "\033[1;31mошибка\033[0m") << std::endl;
}

CalculData::~CalculData()
{
    std::cout << "I: Завершился модуль CalculData >>>" << '\n';
}

int CalculData::CalculCalories()
{
    int BMR(0);

    if (initData.Sex)
        BMR = (88.36 + (13.4 * initData.CurrentWeight) + (4.8 * initData.Growth) - (5.7 * initData.Age));
    else
        BMR = (447.6 + (9.2 * initData.CurrentWeight) + (3.1 * initData.CurrentWeight) - (4.3 * initData.CurrentWeight));

    return (BMR * (initData.Activity == -1 ? 1.375 : initData.Activity == 0 ? 1.55 : 1.725));
}

int CalculData::CalculWater()
{
    return initData.CurrentWeight * ((initData.Sex) ? 40 : 30);
}

int CalculData::CalculFats()
{
    return (calculData.Calories / 6.0) / 9.0;
}

int CalculData::CalculCarbon()
{
    return ((calculData.Calories / 6.0) * 4.0) / 4.0;
}

int CalculData::CalculProteins()
{
    return (calculData.Calories / 6.0) / 4.0;
}

void CalculData::read_file_calcul_data()
{
    if (!(ass::FileExists(FILE_USER_CALCUL_DATA_JSON)))
    {
        system("mkdir -p " FOLDER_USR " " FOLDER_USR_DATA);
        std::ofstream ofs(FILE_USER_CALCUL_DATA_JSON, std::ios_base::binary);
        ofs << Json::object();
        ofs.close();

        m_file_calcul_data_json = Json::object();
    }
    else
    {
        std::ifstream ifs(FILE_USER_CALCUL_DATA_JSON, std::ios_base::binary);
        ifs >> m_file_calcul_data_json;
        ifs.close();
    }
}
